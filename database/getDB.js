const con = require('./../config/db/getConnection.js');

exports.getBotMessage = async function (idMessage){
    let query = `select * from helper_bot.bot_message where categoryId = ${idMessage}`;

    const promise = await new Promise((res, rej) => {
      con.query(query, function (err, result) {
        if (err) {
          rej(err);
        };
        res(result);
      });
    })
    return promise;
}

exports.getKnownWords = async function (){
  let query = `select * from helper_bot.known_words`;

  const promise = await new Promise((res, rej) => {
    con.query(query, function (err, result) {
      if (err) {
        rej(err);
      };
      res(result);
    });
  })
  return promise;
}

exports.getUserByMail = async function (userEmail){
  let query = `select * from helper_bot.user where email='${userEmail}' `;
  
  const promise = await new Promise((res, rej) => {
    con.query(query, function (err, result) {
      if (err) {
        rej(err);
      };
      res(result);
    });
  })

  return promise;
}