drop database if exists helper_bot;
CREATE database helper_bot ;

-- CREATE USER if not exists 'botadmin'@'localhost' IDENTIFIED BY 'He.Bo_19';
-- ALTER USER 'botadmin'@'localhost' IDENTIFIED WITH mysql_native_password BY 'He.Bo_19';
-- GRANT ALL PRIVILEGES ON * . * TO 'botadmin'@'localhost';

-- CREATE USER if not exists 'botselect'@'localhost' IDENTIFIED BY 'He.Bo_19';
-- ALTER USER 'botselect'@'localhost' IDENTIFIED WITH mysql_native_password BY 'He.Bo_19';
-- GRANT SELECT ON helper_bot.* TO 'botselect'@'localhost';

use helper_bot;

CREATE TABLE IF NOT EXISTS helper_bot.role (
    id int auto_increment PRIMARY KEY , 
    roleName TEXT
);

CREATE TABLE IF NOT EXISTS helper_bot.bot_message_category (
    id INT auto_increment PRIMARY KEY,
    categoryName TEXT
);

CREATE TABLE IF NOT EXISTS helper_bot.status (
    id INT auto_increment PRIMARY KEY ,
    statusName TEXT,
    statusLevel int
);

CREATE TABLE IF NOT EXISTS helper_bot.bot_message (
    id int auto_increment PRIMARY KEY,
    categoryId int,
    message TEXT,
    FOREIGN KEY fk_cat(categoryId) REFERENCES bot_message_category(id)
);

CREATE TABLE IF NOT EXISTS helper_bot.user (
    id int auto_increment PRIMARY KEY , 
    fullName TEXT,
    loginName VARCHAR(100) UNIQUE,
    password TEXT,
    email VARCHAR(100) UNIQUE,
    birthDate TIMESTAMP,
    enabled BOOLEAN,
    roleId int,
    FOREIGN KEY fk_role(roleId) REFERENCES role(id)
);

CREATE TABLE IF NOT EXISTS helper_bot.user_status (
    userId int,
    statusId INT,
    statusDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (userId,statusDate),
	FOREIGN KEY user_id(userId) REFERENCES user(id),
    FOREIGN KEY status_id(statusId) REFERENCES status(id)
);


CREATE TABLE IF NOT EXISTS helper_bot.user_chat (
    id int auto_increment PRIMARY KEY,
    userId int,
    message TEXT,
    sender BOOLEAN,
    messageDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY user_fk(userId) REFERENCES user(id)
);

CREATE TABLE IF NOT EXISTS helper_bot.unknown_words (
    id int auto_increment PRIMARY KEY,
    word TEXT,
    timesFound int,
    lastTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS helper_bot.known_words (
    id int auto_increment PRIMARY KEY,
	word TEXT,
    intent TEXT
);

-- #Roles#
INSERT INTO helper_bot.role VALUES (null,"user");
INSERT INTO helper_bot.role VALUES (null,"admin");

-- #Categories message#
INSERT INTO helper_bot.bot_message_category VALUES (null,"checking");

INSERT INTO helper_bot.bot_message_category VALUES (null,"happy");
INSERT INTO helper_bot.bot_message_category VALUES (null,"cheerful");
INSERT INTO helper_bot.bot_message_category VALUES (null,"optimistic");
INSERT INTO helper_bot.bot_message_category VALUES (null,"nice");
INSERT INTO helper_bot.bot_message_category VALUES (null,"regular");
INSERT INTO helper_bot.bot_message_category VALUES (null,"indignant");
INSERT INTO helper_bot.bot_message_category VALUES (null,"angry");
INSERT INTO helper_bot.bot_message_category VALUES (null,"sad");
INSERT INTO helper_bot.bot_message_category VALUES (null,"depression");

-- #Messages#
-- Category checking
insert into helper_bot.bot_message VALUES(null,1,
"Hola de nuevo!");
insert into helper_bot.bot_message VALUES(null,1,
"Que tal te encuentras hoy?");

-- Category happy
insert into helper_bot.bot_message VALUES(null,2, "Me alegro mucho por ti :D, sigue así!");

-- Category cheerful
insert into helper_bot.bot_message VALUES(null,3, "Que bien ;D. Sigue así!");

-- Category optimistic
insert into helper_bot.bot_message VALUES(null,4, "Eso es lo mas importante!");
insert into helper_bot.bot_message VALUES(null,4, "Ser optimista y positivo ayuda mucho en la salud :D");

-- Category nice
insert into helper_bot.bot_message VALUES(null,5, "Muy bien!");
insert into helper_bot.bot_message VALUES(null,5, "Recuerda en ser optimista y positivo para sentirte mejor :D");

-- Category regular
insert into helper_bot.bot_message VALUES(null,6, "Te entiendo");
insert into helper_bot.bot_message VALUES(null,6, "Lo que puedes hacer son ejercicios de relajación como meditación, o hacer deporte para sentirte mejor :)");

-- Category indignant
insert into helper_bot.bot_message VALUES(null,7, "No todo el mundo tiene los mismos pesamientos");
insert into helper_bot.bot_message VALUES(null,7, "Lo que puedes hacer es una actividad que te puede ayudar, rompe un papel en pedazos pequeños");
insert into helper_bot.bot_message VALUES(null,7, "D esta forma, te ayudara a relajarte y despejar tu mente :D");

-- Category angry
insert into helper_bot.bot_message VALUES(null,8, "Estar enojado es algo muy normal del ser humano, aquí te dejo algunos consejos para calmarte:");
insert into helper_bot.bot_message VALUES(null,8, "· Tómate un tiempo para reflexiona, date pequeños descansos en los momentos del día que tienden a ser estresante");
insert into helper_bot.bot_message VALUES(null,8, "· Practica técnicas de relajación, puedes escuchar música, escribir un diario o practicar algunas posturas de yoga: lo que sea necesario para relajarte.");
insert into helper_bot.bot_message VALUES(null,8, "De esta forma tienes ideas para poder relajarte y sentirte mucho mejor :D");

-- Category sad
insert into helper_bot.bot_message VALUES(null,9, "No pasa nada, aquí tienes consejos para sentirte mejor:");
insert into helper_bot.bot_message VALUES(null,9, "· Llora si es necesario, llorar puede ser relajante y puede ayudarte a liberar endorfinas");
insert into helper_bot.bot_message VALUES(null,9, "· Haz algo que te guste como leer, nadar o jugar a las cartas, las actividades que nos gustan hacen sentirte mejor");
insert into helper_bot.bot_message VALUES(null,9, "Con estos consejos mejoraras y seras mas positivo/a :D");

-- Category depression
insert into helper_bot.bot_message VALUES(null,10,
"No pasa nada, en estos casos lo más recomendable es contactar con un psicólogo");
insert into helper_bot.bot_message VALUES(null,10,
"· Busca apoyo, cuando está deprimido es muy importante apoyarse y dejarse ayudar por sus personas más cercanas");
insert into helper_bot.bot_message VALUES(null,10,
"· Haz algo de ejercicio, el simple hecho de realizarlo sienta bien y aumenta el estado de ánimo.");
insert into helper_bot.bot_message VALUES(null,10,
"Con dichos consejos, podras encontrar la forma de sentirte mejor :)");

-- #Status#
INSERT INTO helper_bot.status VALUES (9,"Feliz",1);
INSERT INTO helper_bot.status VALUES (8,"Alegre",1);
INSERT INTO helper_bot.status VALUES (7,"Optimista",1);
INSERT INTO helper_bot.status VALUES (6,"Bien",1);
INSERT INTO helper_bot.status VALUES (5,"Regular",1);
INSERT INTO helper_bot.status VALUES (4,"Indignado",1);
INSERT INTO helper_bot.status VALUES (3,"Enfadado",1);
INSERT INTO helper_bot.status VALUES (2,"Triste",1);
INSERT INTO helper_bot.status VALUES (1,"Deprimido",1);
select * from helper_bot.bot_message where categoryId = 7;
-- #Known words#
-- Deprimido
INSERT INTO helper_bot.known_words VALUES (null, "estoy deprimido", "estado.deprimido");
INSERT INTO helper_bot.known_words VALUES (null, "me siento deprimido", "estado.deprimido");
INSERT INTO helper_bot.known_words VALUES (null, "deprimido", "estado.deprimido");
INSERT INTO helper_bot.known_words VALUES (null, "deprimida", "estado.deprimido");
INSERT INTO helper_bot.known_words VALUES (null, "me siento totalmente deprimida", "estado.deprimido");
INSERT INTO helper_bot.known_words VALUES (null, "necesito ayuda, estoy deprimida", "estado.deprimido");

-- Triste
INSERT INTO helper_bot.known_words VALUES (null, "me encuentro triste", "estado.triste");
INSERT INTO helper_bot.known_words VALUES (null, "triste", "estado.triste");
INSERT INTO helper_bot.known_words VALUES (null, "tristeza", "estado.triste");
INSERT INTO helper_bot.known_words VALUES (null, "me siento muy triste", "estado.triste");
INSERT INTO helper_bot.known_words VALUES (null, "siento que estoy muy triste", "estado.triste");
INSERT INTO helper_bot.known_words VALUES (null, "estoy triste", "estado.triste");
INSERT INTO helper_bot.known_words VALUES (null, "siento tristeza en mi interior", "estado.triste");

-- Enfadado
INSERT INTO helper_bot.known_words VALUES (null, "me encuentro enfadado", "estado.enfadado");
INSERT INTO helper_bot.known_words VALUES (null, "enfadado", "estado.enfadado");
INSERT INTO helper_bot.known_words VALUES (null, "enfado", "estado.enfadado");
INSERT INTO helper_bot.known_words VALUES (null, "cabrada", "estado.enfadado");
INSERT INTO helper_bot.known_words VALUES (null, "enfadada", "estado.enfadado");
INSERT INTO helper_bot.known_words VALUES (null, "rabia", "estado.enfadado");
INSERT INTO helper_bot.known_words VALUES (null, "siento mucha rabia", "estado.enfadado");
INSERT INTO helper_bot.known_words VALUES (null, "siento que estoy muy enfadado", "estado.enfadado");
INSERT INTO helper_bot.known_words VALUES (null, "estoy muy cabreado", "estado.enfadado");

-- Indignado
INSERT INTO helper_bot.known_words VALUES (null, "me encuentro indignado", "estado.indignado");
INSERT INTO helper_bot.known_words VALUES (null, "indignado", "estado.indignado");
INSERT INTO helper_bot.known_words VALUES (null, "indignada", "estado.indignado");
INSERT INTO helper_bot.known_words VALUES (null, "estoy muy indignada", "estado.indignado");
INSERT INTO helper_bot.known_words VALUES (null, "me siento muy indignado", "estado.indignado");
INSERT INTO helper_bot.known_words VALUES (null, "siento que estoy muy indignado", "estado.indignado");
INSERT INTO helper_bot.known_words VALUES (null, "estoy muy indignado", "estado.indignado");

-- Regular
INSERT INTO helper_bot.known_words VALUES (null, "estoy regular", "estado.regular");
INSERT INTO helper_bot.known_words VALUES (null, "podria estar mejor", "estado.regular");
INSERT INTO helper_bot.known_words VALUES (null, "ni tan mal", "estado.regular");
INSERT INTO helper_bot.known_words VALUES (null, "regulin", "estado.regular");
INSERT INTO helper_bot.known_words VALUES (null, "regular", "estado.regular");
INSERT INTO helper_bot.known_words VALUES (null, "normal", "estado.regular");

-- Bien
INSERT INTO helper_bot.known_words VALUES (null, "bien", "estado.bien");
INSERT INTO helper_bot.known_words VALUES (null, "estoy bien", "estado.bien");
INSERT INTO helper_bot.known_words VALUES (null, "me siento bien", "estado.bien");
INSERT INTO helper_bot.known_words VALUES (null, "me encuentro francamente bien", "estado.bien");
INSERT INTO helper_bot.known_words VALUES (null, "contento", "estado.bien");
INSERT INTO helper_bot.known_words VALUES (null, "estoy contento", "estado.bien");
INSERT INTO helper_bot.known_words VALUES (null, "la verdad, es que me siento bien", "estado.bien");

-- Optimista
INSERT INTO helper_bot.known_words VALUES (null, "optimista", "estado.optimista");
INSERT INTO helper_bot.known_words VALUES (null, "hoy me siento muy optimista", "estado.optimista");
INSERT INTO helper_bot.known_words VALUES (null, "optimismo", "estado.optimista");
INSERT INTO helper_bot.known_words VALUES (null, "muy bien", "estado.optimista");
INSERT INTO helper_bot.known_words VALUES (null, "me encuentro bastante bien", "estado.optimista");
INSERT INTO helper_bot.known_words VALUES (null, "estoy optimista", "estado.optimista");

-- Alegre
INSERT INTO helper_bot.known_words VALUES (null, "alegre", "estado.alegre");
INSERT INTO helper_bot.known_words VALUES (null, "hoy me siento muy alegre", "estado.alegre");
INSERT INTO helper_bot.known_words VALUES (null, "me encuentro bastante alegre", "estado.alegre");
INSERT INTO helper_bot.known_words VALUES (null, "estoy alegre", "estado.alegre");

-- Feliz
INSERT INTO helper_bot.known_words VALUES (null, "feliz", "estado.feliz");
INSERT INTO helper_bot.known_words VALUES (null, "estoy muy feliz", "estado.feliz");
INSERT INTO helper_bot.known_words VALUES (null, "me encuentro muy feliz", "estado.feliz");
INSERT INTO helper_bot.known_words VALUES (null, "estoy feliz", "estado.feliz");
INSERT INTO helper_bot.known_words VALUES (null, "me siento increiblemente feliz", "estado.feliz");

-- #Users#
insert into helper_bot.user VALUES (null,"Usuario","usuario",MD5('1234'),"usuario@gmail.com",CURRENT_TIMESTAMP,1,1 );
insert into helper_bot.user VALUES (null,"Administrador","admin",MD5('1234'),"admin@gmail.com",CURRENT_TIMESTAMP,1,2 );

-- #Status#
insert into helper_bot.user_status VALUES ("1","5",CURRENT_TIMESTAMP+100);
insert into helper_bot.user_status VALUES ("1","3",CURRENT_TIMESTAMP+200);
insert into helper_bot.user_status VALUES ("1","6",CURRENT_TIMESTAMP+300);
insert into helper_bot.user_status VALUES ("1","4",CURRENT_TIMESTAMP+400);

-- #Unknown words#
insert into helper_bot.unknown_words VALUES (null,"estupefacto",1,'2016-06-10');
insert into helper_bot.unknown_words VALUES (null,"consternido",2923,'2019-06-10');
insert into helper_bot.unknown_words VALUES (null,"agazapado",2923,'2019-06-11');