const con = require('./../config/db/modifyConnection.js');

exports.saveUnknownWords = async function (message){
  const existsThisMessage = await checkIfExistsWord(message);

    if (existsThisMessage.length != 0){
      await updateCountWord(message);
    } else {
      let query = `insert into helper_bot.unknown_words VALUES (null,"${message}",1, CURRENT_TIMESTAMP);`;

      con.query(query, function (err, result) {
        if (err) throw err;
      });
    }
}

async function checkIfExistsWord(message){
  const query = `SELECT * FROM helper_bot.unknown_words WHERE word = '${message}';`;

  const promise = await new Promise((res, rej) => {
    con.query(query, function (err, result) {
      if (err) {
        rej(err);
      };
      res(result);
    });
  })
  return promise;
}

function updateCountWord(message){
  const query = `UPDATE helper_bot.unknown_words SET timesFound = timesFound + 1 WHERE word = '${message}';`;
  
  con.query(query, function (err, result) {
    if (err) throw err;
  });
}