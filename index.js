// Cargar variables de entorno en process.env con los valores de archivo .env
require('dotenv').config();
const express = require("express");
const app = express();
const cors = require('cors');
const passport = require("passport");
const constants = require("./constants.js");
const authRoutes = require("./routes/authRoutes.js");
const getFromDB = require("./routes/getFromDB.js");
const modifyDBData = require("./routes/modifyDBData.js");
const iaUtils = require('./routes/IAUtils.js');
const bodyParser = require("body-parser");
const trainIA = require('./config/IA/configTrainNLP.js');

//Train IA when server starts
trainIA();

//Enable CORS
app.use(cors());

//Paspport Config
require("./config/passport.js");

//BotKit Config
require("./config/botkit.js");

//Without this you can't get data of request
app.use(bodyParser.urlencoded({ extended: false }));

//Start passport
app.use(passport.initialize());

//Configure  all endpoints in routes.js for works in /auth/...
app.use("/auth", authRoutes);

app.use("/bbdd", getFromDB);

app.use("/bbdd", modifyDBData);

app.use("/ia", iaUtils);

app.listen(constants.PORTSERVER, function() {
  console.log("AuthServer listening on port:" + constants.PORTSERVER);
});
