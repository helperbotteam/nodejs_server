const express = require("express");
const passport = require("passport");
const { SECRET_JWT, TOKEN_EXPIRE } = require("../constants.js");
const jwt = require("jsonwebtoken");
const path = express.Router();

//End Point with verify Logic
path.post("/verify", function(req, res) {
  if (req.headers.authorization) {
    let token = req.headers.authorization.split(" ")[1];
    if (token) {
      try {
        jwt.verify(token, SECRET_JWT, function(err, decoded) {
          if (err) {
            return res.json({
              success: false,
              message: "Failed to authenticate token."
            });
          } else {
            // if everything is good return this json
            return res.json({ success: true, message: "Is verified." });
          }
        });
      } catch (err) {
        res.send(400);
      }
    } else {
      res.send(400);
    }
  } else {
    res.send(401);
  }
});

//End Point with Google Auth
path.get(
  "/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);
path.get("/facebook", passport.authenticate("facebook",{ scope: ['email', 'public_profile'] }));

path.get(
  "/google/callback",
  passport.authenticate("google", {
    failureRedirect: "/auth/google"
  }),
  function(req, res) {
    let token = jwt.sign(
      {
        profile: req.user
      },
      SECRET_JWT,
      { expiresIn: TOKEN_EXPIRE }
    );
    // Successful authentication, send Token.
    res.redirect(`${process.env.FRONT_URL}/login?token=${token}`);
  }
);

path.get(
  "/facebook/callback",
  passport.authenticate("facebook", {
    failureRedirect: "/auth/facebook"
  }),
  function(req, res) {
    let token = jwt.sign(
      {
        profile: req.user
      },
      SECRET_JWT,
      { expiresIn: TOKEN_EXPIRE }
    );
    // Successful authentication, send Token.
    res.redirect(`${process.env.FRONT_URL}/login?token=${token}`);
  }
);

module.exports = path;
