const con = require('./../config/db/getConnection.js');
const express = require("express");
const bodyParser = require("body-parser");
const app = express.Router();
const { SECRET_JWT, TOKEN_EXPIRE } = require("../constants.js");
const jwt = require("jsonwebtoken");
const CryptoJS = require("crypto-js");
const { getBotMessage } = require('../database/getDB.js');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json());      
app.use(express.urlencoded());

app.post("/getUser", function(request, response) {
  let userID = request.body.data.userId;
  let query = `select * from helper_bot.user where id='${userID}' `;

  con.query(query, function (err, result) {
      if (err) throw err;
      response.json(result)
  });
});

app.post("/getUserByMail", function(request, response) {
  let userEmail = request.body.data.userEmail;
  let query = `select * from helper_bot.user where email='${userEmail}' `;

  con.query(query, function (err, result) {
      if (err) throw err;
      response.json(result)
  });
});

app.post("/checkUser", function(request, response) {
  let username = request.body.data.username;
  let password = request.body.data.password;

  if(username && password) {
    let query = `select password,email from helper_bot.user where loginName='${username}' `;

    con.query(query, function (err, result) {
        if (err) throw err;

        if (result[0] && result[0].password == password) {
          let token = jwt.sign(
            {
              profile: {
                emails: [
                  {
                    value: result[0].email
                  }
                ]
              }
            },
            SECRET_JWT,
            { expiresIn: TOKEN_EXPIRE }
          );
          response.json(token)
        } else {
          response.json("Login error")
        }
    });
  } else {
    response.json("Login error")
  }
  
});

app.post("/checkSocialUser", function(request, response) {
  let userEmail = request.body.data.userEmail;
  let query = `select * from helper_bot.user where email = '${userEmail}'`;

  con.query(query, function (err, result) {
      if (err) throw err;
      if(result.length == 0){
        response.json("false")
      } else {
        response.json("true")
      }
      
  });
});

app.post("/getUserStatus", function(request, response) {
  let userID = request.body.data.userId;
  let query = `select statusId as y, statusDate as x from helper_bot.user_status WHERE userId='${userID}' `;

  con.query(query, function (err, result) {
      if (err) throw err;
      response.json(result)
  });
});

app.post("/getUserChat", function(request, response) {
  let userID = request.body.data.userId;
  let query = `SELECT * FROM (
    select message, sender, messageDate  from helper_bot.user_chat where userId = '${userID}' order by messageDate desc limit 10
    ) sub
    ORDER BY messageDate ASC`;
  
  con.query(query, function (err, result) {
      if (err) throw err;
      response.json(result)
  });
});

app.post("/getUnknownWords", function(request, response) {
  let query = `select * from helper_bot.unknown_words `;

  con.query(query, function (err, result) {
      if (err) throw err;
      response.json(result)
  });
});

app.post("/getIntents", function(request, response) {
  let query = `SELECT distinct(intent) FROM helper_bot.known_words;`;

  con.query(query, function (err, result) {
      if (err) throw err;
      response.json(result)
  });
});

/*app.post("/getUnknownWords", function(request, response) {
  let initialDate = request.body.data.initialDate;
  let finalDate = request.body.data.finalDate;
  let query = `select * from helper_bot.unknown_words `;
  if (initialDate != null && finalDate != null) {
    query += `where lastTime BETWEEN '${initialDate}' AND '${finalDate}'`
  }

  con.query(query, function (err, result) {
      if (err) throw err;
      response.json(result)
  });
});*/

app.post("/getStatus", function(request, response) {
  let statusId = request.body.data.statusId;
  let query = `select * from helper_bot.status where statusLevel = '${statusId}'`;
  con.query(query, function (err, result) {
      if (err) throw err;
      response.json(result)
  });
});

app.post("/getBotMessage", async function(request, response) {
  let idMessage = request.body.data.idMessage;
  const result = await getBotMessage(idMessage);
  response.json(result);
});

module.exports = app;
