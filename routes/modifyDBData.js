const con = require("./../config/db/modifyConnection.js");
const express = require("express");
const bodyParser = require("body-parser");
const app = express.Router();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/saveUserStatus", function(request, response) {
    const userID = request.body.data.userId;
    const statusId = request.body.data.statusId;
    const query = `insert into helper_bot.user_status VALUES ("${userID}","${statusId}",CURRENT_TIMESTAMP)`;

    con.query(query, function (err, result) {
        if (err) throw err;
        response.json("Insert OK")
    });
});

app.post("/disableUser", function(request, response) {
    const userID = request.body.data.userId;
    const query = `update helper_bot.user set enabled = 0 where id = ${userID}`;

    con.query(query, function (err, result) {
        if (err) throw err;
        response.json("User disabled")
    });
});

app.post("/createUser", function(request, response) {
    const userData = JSON.parse(request.body.data.userData);
    if (userData) {
        let query = `insert into helper_bot.user VALUES (null,"${userData.fullname}","${userData.username}",MD5('${userData.password}'),"${userData.email}",CURRENT_TIMESTAMP,1,1)`;
        //if user is from social media
        if (userData.password == null) {
            query = `insert into helper_bot.user VALUES (null,"${userData.fullname}","${userData.username}",null,"${userData.email}",CURRENT_TIMESTAMP,1,1)`;
        }
        con.query(query, function (err, result) {
            if (err) throw err;
            response.json("User created")
        });
    }
    
});

app.post("/insertKnownWords", function(request, response) {
    const word = request.body.data.word;
    const intent = request.body.data.intent;
    const query = `insert into helper_bot.known_words values (null,'${word}','${intent}') `;
  
    con.query(query, function (err, result) {
        if (err) throw err;
        response.json("Insert OK");
    });
});

app.post("/saveToChat", function(request, response) {
    const messageData = JSON.parse(request.body.data.messageData);
    const query = `insert into helper_bot.user_chat VALUES (null, ${messageData.userId},"${messageData.message}",${messageData.sender},CURRENT_TIMESTAMP);`;
  
    con.query(query, function (err, result) {
        if (err) throw err;
        response.json("Insert OK");
    });
});

app.post("/removeUnknownWords", function(request, response) {
    const id = request.body.data.id;
    const query = `DELETE FROM helper_bot.unknown_words WHERE id=${id};`;

    con.query(query, function (err, result) {
        if (err) throw err;
        response.json("Removed");
    });
});
  
app.post("/insertUserStatus" , function(request, response) {
    const userId = request.body.data.userId;
    const statusId = request.body.data.statusId;
    const query = `insert into helper_bot.user_status values ('${userId}', ${statusId}, CURRENT_TIMESTAMP)`;
    
    con.query(query, function (err, result) {
        if (err) throw err;
        response.json("Insert OK");
    });
})

app.post("/updateUser", function(request, response) {
    const userData = JSON.parse(request.body.data.userData);
    const query = `UPDATE helper_bot.user SET loginName = "${userData.newName}", password= MD5('${userData.newPassword}') WHERE loginName = "${userData.oldName}"`;

    con.query(query, function (err, result) {
        if (err) throw err;
        response.json("User modified")
    });
});

module.exports = app;
