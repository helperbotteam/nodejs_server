const express = require("express");
const bodyParser = require("body-parser");
const app = express.Router();
const trainIA = require('./../config/IA/configTrainNLP.js');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded());

app.get("/train", async function (request, response) {
    await trainIA();
    response.json(true);
});

module.exports = app;