const mysql = require('mysql');

const con = mysql.createConnection({
    host: "localhost",
    user: process.env.DB_USER,
    password: process.env.DB_PASSWD
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

module.exports = con;