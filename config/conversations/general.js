const { sendTextToClient } = require("./../utils.js");
const { saveUnknownWords } = require("./../../database/modifyDB.js");
const { INVALID_REQUEST } = require("./../../constants.js");
module.exports = function (controller) {

  controller.hears ('none', 'message', async (bot, message) => {
    await saveUnknownWords(message.orginalText);
    await bot.reply(message, sendTextToClient(["Lo siento, no te he entendido"],[]));
  });

  controller.hears (INVALID_REQUEST, 'message', async (bot, message) => {
    await bot.reply(message, sendTextToClient([INVALID_REQUEST]));
  });
};
