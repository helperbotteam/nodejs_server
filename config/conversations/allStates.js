const {
    sendTextToClient
} = require("../utils.js");
const {
    getBotMessage
} = require('../../database/getDB.js');
const {
    insertUserStatus
} = require('./../../database/modifyDB.js');

module.exports = function (controller) {
    controller.hears("estado.feliz", "message", async (bot, message) => {
        const data = await getBotMessage(2);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });

    controller.hears("estado.alegre", "message", async (bot, message) => {
        const data = await getBotMessage(3);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });

    controller.hears("estado.optimista", "message", async (bot, message) => {
        const data = await getBotMessage(4);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });

    controller.hears("estado.bien", "message", async (bot, message) => {
        const data = await getBotMessage(5);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });

    controller.hears("estado.regular", "message", async (bot, message) => {
        const data = await getBotMessage(6);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });

    controller.hears("estado.indignado", "message", async (bot, message) => {
        const data = await getBotMessage(7);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });

    controller.hears("estado.enfadado", "message", async (bot, message) => {
        const data = await getBotMessage(8);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });

    controller.hears("estado.triste", "message", async (bot, message) => {
        const data = await getBotMessage(9);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });

    controller.hears("estado.deprimido", "message", async (bot, message) => {
        const data = await getBotMessage(10);
        const lastMessage = data.pop();
        
        for (const eachMessage of data) {
            await bot.reply(message, sendTextToClient([eachMessage.message]));
        }

        await bot.reply(message, sendTextToClient([lastMessage.message],[]));
    });
}