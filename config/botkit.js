const {
  Botkit
} = require('botkit');
const {
  WebAdapter
} = require('botbuilder-adapter-web');
const jwt = require("jsonwebtoken");
const {
  SECRET_JWT, INVALID_REQUEST
} = require("./../constants.js");
const {
  getUserByMail
} = require('./../database/getDB.js');
const adapter = new WebAdapter({
  path: '/helperbot',
});
const controller = new Botkit({
  adapter: adapter
});
const { getClassifications } = require('./IA/NLPBot.js');

/* IA Middleware */
async function applyNLPLogicalMiddleware(bot, message, next) {
  const token = message.user.token;

  try {
      const emailUser = await checkTokenAndGetEmailUser(token);
      const {
        intent
      } = await getClassifications(message.text);
  
      message.orginalText = message.text;
      message.text = intent.toLowerCase();
  
      message.user = await getUserByMail(emailUser);
  } catch(e) {
    console.error("Error in Botkit:", e);
    message.text = INVALID_REQUEST;
  }

  next();
}

function checkTokenAndGetEmailUser(token) {
  return new Promise((res, rej) => {
    jwt.verify(token, SECRET_JWT, function (err, decoded) {
      if (err) {
        rej("Invalid token");
      } else {
        res(decoded.profile.emails[0].value);
      }
    })
  });
}

controller.middleware.ingest.use(applyNLPLogicalMiddleware);

controller.ready(() => {
  // Carga las caracteristicas del chatbot en el controller
  controller.loadModules(__dirname + '/conversations');
});