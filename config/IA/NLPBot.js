const { NlpManager } = require('node-nlp');

const manager = new NlpManager({ languages: ['es'] });

exports.getClassifications = async function (userMessage) {
    manager.load(__dirname + '/model.nlp');

    return await manager.process(userMessage);
};