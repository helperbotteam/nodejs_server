const { NlpManager } = require ('node-nlp');
const { getKnownWords } = require('./../../database/getDB.js');

const language = 'es';

const manager = new NlpManager ({languages: [language]});

module.exports = async function() {
  const allKnownWords = await getKnownWords();

  if (allKnownWords){
    allKnownWords.map(({word, intent}) => {
      manager.addDocument(language, word, intent);
    })
  
    await manager.train ();
    manager.save (__dirname + '/model.nlp');
  }  
}