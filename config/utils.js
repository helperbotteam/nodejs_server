/*
  messages: Array with strings for send a messages.

  quickReplaysJSON: JSON on show all posibles replays for the client to bot.
  When quickReplaysJSON is null, in front dont show message until recive a replays without null.

  suggeretionsComponent: String on have a Quasar component for display in front
*/
exports.sendTextToClient = function (messages, quickRepliesJSON = null){ 
    const messageFormat = {
      messages: messages,
      quickReplies: quickRepliesJSON,
    }

    return { text: messageFormat };
}